<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/stylesheet/main.css"/>
    <title>Netshoes Challenge</title>
</head>
<body>
<div class="container">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <li role="presentation"><a href="/address">Index</a></li>
                <li role="presentation" class="active"><a href="/address/novo">Novo</a></li>
            </ul>
        </nav>
        <h3 class="text-muted">Netshoes Challenge</h3>
    </div>
    <div class="main-content">
        <c:if test="${not empty errors}">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <c:forEach var="error" items="${errors}">
                    ${error.category} - ${error.message}<br />
                </c:forEach>
            </div>
        </c:if>

        <div class="alert alert-warning alert-dismissible" id="alert" style="display: none" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            CEP Não encontrado.
        </div>
        <form action="/address" method="post" class="form-horizontal">
            <input type="hidden" name="address.id" value="${address.id}" />

            <div class="form-group">
                <label for="cep" class="col-md-offset-2 col-md-2 control-label">CEP:</label>

                <div class="col-md-4">
                    <input id="cep" class="form-control" type="text" maxlength="9" name="address.cep" value="${address.cep}" />
                </div>

                <button id="findCep" class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"/></button>
            </div>

            <div class="form-group">
                <label for="numero" class="col-md-offset-2 col-md-2 control-label">Numero:</label>
                <div class="col-md-4">
                    <input id="numero" class="form-control" type="text" name="address.numero" value="${address.numero}" />
                </div>
            </div>

            <div class="form-group">
                <label for="bairro" class="col-md-offset-2 col-md-2 control-label">Bairro:</label>
                <div class="col-md-4">
                    <input id="bairro" class="form-control" type="text" name="address.bairro" value="${address.bairro}" />
                </div>
            </div>

            <div class="form-group">
                <label for="cidade" class="col-md-2 col-md-offset-2 control-label">Cidade:</label>
                <div class="col-md-4">
                    <input id="cidade" type="text" class="form-control" name="address.cidade" value="${address.cidade}" />
                </div>
            </div>

            <div class="form-group">
                <label for="estado" class="col-md-2 col-md-offset-2 control-label">Estado:</label>
                <div class="col-md-4">
                    <input id="estado" type="text" class="form-control" name="address.estado" value="${address.estado}" />
                </div>
            </div>

            <div class="form-group">
                <label for="rua" class="col-md-2 col-md-offset-2 control-label">Rua:</label>
                <div class="col-md-4">
                    <input id="rua" type="text" class="form-control" name="address.logradouro" value="${address.logradouro}" />
                </div>
            </div>

            <div class="form-group">
                <label for="complemento" class="col-md-offset-2 col-md-2 control-label">Complemento:</label>
                <div class="col-md-4">
                    <input id="complemento" class="form-control" type="text" name="address.complemento" value="${address.complemento}" />
                </div>
            </div>
            <button type="submit" class="btn btn-primary col-md-offset-9">Salvar</button>
        </form>

    </div>
</div>
<ul>

</ul>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="/assets/javascript/jquery.mask.min.js"></script>
<script src="/assets/javascript/cep-search.js"></script>
</body>
</html>

