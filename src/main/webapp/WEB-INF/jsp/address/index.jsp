<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/stylesheet/main.css"/>
    <title>Netshoes Challenge</title>
</head>
<body>
    <div class="container">
        <div class="header clearfix">
           <nav>
               <ul class="nav nav-pills pull-right">
                   <li role="presentation" class="active"><a href="/address">Index</a></li>
                   <li role="presentation"><a href="/address/novo">Novo</a></li>
               </ul>
           </nav>
           <h3 class="text-muted">Netshoes Challenge</h3>
        </div>
        <div class="main-content">
            <c:if test="${not empty errors}">
                <div class="alert alert-warning alert-dismissible" role="alert" >
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <c:forEach var="error" items="${errors}">
                        ${error.category} - ${error.message}<br />
                    </c:forEach>
                </div>
            </c:if>


            <div class="alert alert-warning alert-dismissible" role="alert" style="display: none" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                Endereço Removido com sucesso.
            </div>
            <form class="form-inline" action="/address">
                <div class="input-group pull-right">
                    <input type="text" class="form-control" placeholder="Pesquisa por ID" name="address.id"
                            value="${address.id}">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Rua</th>
                        <th>Bairro</th>
                        <th>Cidade</th>
                        <th>Estado</th>
                        <th>CEP</th>
                        <th></th>
                    </tr>
                </thead>
                <c:forEach items="${addresses}" var="address">
                    <tr>
                        <td>${address.id}</td>
                        <td>${address.logradouro}</td>
                        <td>${address.bairro}</td>
                        <td>${address.cidade}</td>
                        <td>${address.estado}</td>
                        <td>${address.cep}</td>
                        <td><a href="/address/${address.id}"><span class="glyphicon glyphicon-edit"></span></a></td>
                        <td><a href="#" data-address-id="${address.id}" class="delete"><span class="glyphicon glyphicon-trash"></span></a></td>

                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>
<ul>

</ul>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var answer = confirm('Deseja remover o Endereço ? ');
            if(answer  == true){
                var $element = $(this);
                var addressId = $element.data('address-id');
                $.ajax({
                    url: '/address/'+ addressId,
                    type: 'DELETE',
                    success: function(result) {
                        $element.closest('tr').hide();
                        $('#alert').show();
                    }
                });
            }
        })
    })
</script>
</body>
</html>
