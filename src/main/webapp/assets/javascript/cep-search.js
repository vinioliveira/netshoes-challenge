$(function() {
    $('#cep').mask('00000-000');
    $('#findCep').click(function () {
        var cep = $('#cep').cleanVal();
        if(cep === undefined || cep == '') return;
        //Esta como localhost mas poderia ser qualqer serviço
        //externo com JSONP
        $.getJSON("/v1/cep/"+ cep, function (address) {
            if(address != "Invalid CEP") {
                $('#bairro').val(address.bairro);
                $('#cidade').val(address.cidade);
                $('#estado').val(address.estado);
                $('#rua').val(address.logradouro);
            }else{
                $('#alert').show();
            }
        })
    })
});
