package com.vinioliveira.controller;

import br.com.caelum.vraptor.*;
import br.com.caelum.vraptor.view.Results;
import com.vinioliveira.model.Address;
import com.vinioliveira.service.CEPService;
import com.vinioliveira.service.CEPService.CEPInvalidException;

import javax.inject.Inject;

@Controller
@Path("/v1")
public class ApiCEPController {
    @Inject private CEPService service;
    @Inject private Result result;

    @Consumes("application/json")
    @Get("cep/{cep:[0-9]{8}}")
    public void index(String cep){
        try{
            Address address = service.fetch(cep);
            result.use(Results.json()).withoutRoot().from(address).serialize();
        }catch (CEPInvalidException e){
            result.use(Results.json()).withoutRoot().from(e.getMessage()).serialize();
        } catch (CEPService.CEPServiceException e) {
            result.use(Results.json()).withoutRoot().from(e.getMessage()).serialize();
        }

    }
}
