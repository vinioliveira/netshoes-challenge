package com.vinioliveira.controller;

import br.com.caelum.vraptor.*;
import br.com.caelum.vraptor.validator.Validator;
import com.vinioliveira.model.Address;
import com.vinioliveira.repository.AddressRepository;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

@Controller
public class AddressController {

    @Inject private Validator validator;
    @Inject private AddressRepository service;
    @Inject private Result result;

    @Get("/address")
    public void index(Address address) {
        try {
            result.include("addresses", service.find(address)).include("address", address);
        }catch(PersistenceException e){
            result.include("error", e.getMessage());
        }
    }

    @Get("/address/novo")
    public void novo(){}

    @Post("/address")
    public void save(Address address) {
        try {
            validate(address).onErrorRedirectTo(this).novo();
            service.save(address);
            result.include("message", "Address salvo com sucesso!").redirectTo(this).index(null);
        } catch (PersistenceException e) {
            e.printStackTrace();
            result.include("error", e.getMessage()).redirectTo(this).novo();
        }
    }

    @Get("/address/{address.id}")
    public void view(Address address) {
        try {
            address = service.find(address.getId());
            result.include("address", address);
        } catch (PersistenceException e) {
            e.printStackTrace();
            result.redirectTo(this).index(null);
        }
    }

    @Delete("/address/{address.id}")
    public void remove(Address address) {
        try {
            service.remove(address.getId());
            result.include("message", "Address excluido com sucesso!").forwardTo(this).index(null);
        } catch (PersistenceException e) {
            e.printStackTrace();
            result.include("message", "Error ao excluir address");
        }
    }

    private Validator validate(Address model) {
        return validator.validate(model);
    }
}
