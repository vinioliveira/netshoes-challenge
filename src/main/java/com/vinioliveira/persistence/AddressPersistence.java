package com.vinioliveira.persistence;

import br.com.caelum.vraptor.validator.Validator;
import com.vinioliveira.model.Address;
import com.vinioliveira.repository.AddressRepository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

public class AddressPersistence implements AddressRepository {

    @Inject private EntityManager em;
    @Inject private Validator validator;
    private Class<Address> clazz = Address.class;
    private Criteria criteria;

    public Address find(Long id) throws PersistenceException {
        try {
            return em.find(clazz, id);
        } catch (PersistenceException e) {
            throw new PersistenceException("Address não encontrado", e);
        }
    }

    public Address save(Address model) throws PersistenceException {
        try {
            validate(model);
            return em.merge(model);
        } catch (Exception e) {
            throw new PersistenceException("Erro ao salvar o Address", e);
        }
    }

    private void validate(Address model) {
        validator.validate(model);
        validator.onErrorSendBadRequest();
    }

    public List<Address> find(Address address) throws PersistenceException {
        try {
            if (address != null && address.getId() != null){
                criteria().add(Restrictions.eq("id", address.getId()));
            }
            return criteria().list();
        } catch (Exception e) {
            throw new PersistenceException("Erro ao buscar todos os addresses", e);
        }
    }

    public List<Address> all() throws PersistenceException {
        try {
            return criteria().list();
        } catch (Exception e) {
            throw new PersistenceException("Erro ao buscar todos os addresses", e);
        }
    }

    public void remove(Long id) throws PersistenceException {
        try {
            em.remove(find(id));
        } catch (Exception e) {
            throw new PersistenceException("Erro ao remover address", e);
        }
    }

    private Criteria criteria(){
        if(criteria == null){
           criteria =((Session) em.getDelegate()).createCriteria(Address.class);
        }

        return criteria;
    }
}
