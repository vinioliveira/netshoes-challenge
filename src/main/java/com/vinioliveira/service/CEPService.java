package com.vinioliveira.service;


import com.google.gson.Gson;
import com.vinioliveira.model.Address;
import com.vinioliveira.validators.CEPValidator;

import javax.enterprise.context.RequestScoped;

public class CEPService {

    public CEPService() {
    }

    public CEPService(RestRequest restRequest) {
        this.restRequest = restRequest;
    }

    private String URL_CONSULTA = "http://api.postmon.com.br/v1/cep/";
    private RestRequest restRequest;

    public Address fetch(String cep) throws CEPServiceException, CEPInvalidException {
        try {
            if(!CEPValidator.isValid(cep)) throw new CEPInvalidException("Invalid CEP");

            String json = getRestRequest().get(cep);

            if(json == null || json.isEmpty()) {
                return fetch(replaceDigitsByZero(cep));
            }else{
                Address address = parserJsonAddres(json);
                return address;
            }
        } catch (RestRequest.RestRequestException e) {
            throw new CEPServiceException("Problemas de comunicação com o serviço de CEP");
        }
    }

    private Address parserJsonAddres(String json) {
        return new Gson().fromJson(json, Address.class);
    }

    private static String replaceDigitsByZero(String cep) {
        StringBuilder sbCep = new StringBuilder(cep);
        for (int index = sbCep.length() - 1; index > 0; index--){
            String character = sbCep.substring(index, index + 1);
            if(!character.equals("0") && !character.equals("-")){
                sbCep.setCharAt(index, '0');
                break;
            }
        }
        return sbCep.toString();
    }

    public class CEPInvalidException extends Exception{
        public CEPInvalidException(String message) {
            super(message);
        }
    }

    public class CEPServiceException extends Exception{
        public CEPServiceException(String message) {
            super(message);
        }
    }

    public RestRequest getRestRequest(){
        if(restRequest == null) {
          restRequest = new RestRequest(URL_CONSULTA);
        }
        return restRequest;
    }
}
