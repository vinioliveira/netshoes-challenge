package com.vinioliveira.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


class RestRequest {

    private String url;

    public RestRequest(String url) {
        this.url = url;
    }

    public String get(String param) throws RestRequestException {
        String json = new String();
        try {
            appendToUrl(param);
            HttpURLConnection conn = connect();

            if (conn.getResponseCode() == 200){
                json = parserResult(conn.getInputStream());
            }
        } catch (IOException e) {
            throw new RestRequestException();
        }
        return json;
    }

    private String parserResult(InputStream inputStream) throws RestRequestException {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (inputStream)));
            String line;

            while ((line = br.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            throw new RestRequestException();
        }
        return result.toString();
    }

    private HttpURLConnection connect() throws IOException {
        URL netUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) netUrl.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        return conn;
    }

    private void appendToUrl(String param){
        if(param != null){
            url = url + param;
        }
    }

    public class RestRequestException extends Exception{

    }
}
