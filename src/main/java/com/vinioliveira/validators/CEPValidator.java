package com.vinioliveira.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CEPValidator {
    private static Pattern pattern = Pattern.compile("[0-9]{5}-?[0-9]{3}");
    private static Pattern patternZero = Pattern.compile("[0]{5}-?[0]{3}");

    public static boolean isValid(String cep) {
        if(cep == null || cep.isEmpty()) return  false;
        Matcher defaultPattern = pattern.matcher(cep.trim());
        Matcher onlyZeroPattern = patternZero.matcher(cep.trim());
        return defaultPattern.matches() && !onlyZeroPattern.matches();
    }
}
