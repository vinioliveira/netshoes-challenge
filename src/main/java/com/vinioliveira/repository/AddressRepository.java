package com.vinioliveira.repository;

import com.vinioliveira.model.Address;

import javax.persistence.PersistenceException;
import java.util.List;

public interface AddressRepository {

    public Address find(Long id) throws PersistenceException;

    public List<Address> find(Address address) throws PersistenceException;

    public Address save(Address model) throws PersistenceException;

    public List<Address> all() throws PersistenceException;

    public void remove(Long id) throws PersistenceException;

}
