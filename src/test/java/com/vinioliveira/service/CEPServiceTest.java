package com.vinioliveira.service;


import com.vinioliveira.model.Address;
import static org.mockito.Mockito.*;
import com.vinioliveira.service.CEPService.CEPInvalidException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.*;


public class CEPServiceTest {

    private CEPService service;

    @Before
    public void setUp() throws Exception {
        RestRequest mock = mock(RestRequest.class);
        when(mock.get(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                String cep = (String) args[0];
                if(cep.equals("01151000")){
                    return CEPResponse.VALID_RESULT;
                }else{
                    return null;
                }
            }
        });
        service = new CEPService(mock);
    }

    @Test(expected=CEPInvalidException.class)
    public void testLevantarExecaoCEPInvalido() throws CEPService.CEPServiceException, CEPInvalidException {
        service.fetch("XXXXXXX");
    }

    @Test
    public void testRetornarInformacoesParaUmCepValido() throws CEPService.CEPServiceException, CEPInvalidException {
        Address address = service.fetch("01151000");
        assertEquals("Barra Funda", address.getBairro());
        assertEquals("São Paulo", address.getCidade());
        assertEquals("SP", address.getEstado());
        assertEquals("Rua Brigadeiro Galvão", address.getLogradouro());
    }

    @Test
    public void testAcresentarZerosNoFinalAteEncontarUmValido() throws CEPService.CEPServiceException, CEPInvalidException {
        Address address = service.fetch("01151999");
        assertEquals("Barra Funda", address.getBairro());
        assertEquals("São Paulo", address.getCidade());
        assertEquals("SP", address.getEstado());
        assertEquals("Rua Brigadeiro Galvão", address.getLogradouro());
    }

    @Test(expected=CEPInvalidException.class)
    public void testCEPFormatoValidoComApenasZeros() throws CEPService.CEPServiceException, CEPInvalidException {
        Address address = service.fetch("00000-000");
    }

    @Test(expected=CEPInvalidException.class)
    public void testCEPNaoFormatadoValidoComApenasZeros() throws CEPService.CEPServiceException, CEPInvalidException {
        Address address = service.fetch("00000000");
    }
}