package com.vinioliveira.service;

/**
 * Created by vinioliveira on 3/30/15.
 */
public final class CEPResponse {
    public static String VALID_RESULT = new StringBuilder()
      .append("{\"bairro\": \"Barra Funda\", ")
      .append("\"cidade\": \"São Paulo\",")
      .append("\"cep\": \"01151000\",")
      .append("\"logradouro\": \"Rua Brigadeiro Galvão\",")
      .append("\"estado_info\": ")
      .append("    {\"area_km2\": \"248.222,801\", ")
      .append("     \"codigo_ibge\": \"35\", \"nome\": \"São Paulo\"},")
      .append("     \"cidade_info\": ")
      .append("          {\"area_km2\": \"1521,101\", ")
      .append("           \"codigo_ibge\": \"3550308\"},")
      .append("           \"estado\": \"SP\"}").toString();
}
