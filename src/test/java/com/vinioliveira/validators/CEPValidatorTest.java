package com.vinioliveira.validators;

import org.junit.Before;
import org.junit.Test;

import java.util.regex.Matcher;

import static org.junit.Assert.*;

public class CEPValidatorTest {

    @Test
    public void testCepFormatadoValido() {
        assertTrue( CEPValidator.isValid("01151-000") );
    }

    @Test
    public void testCepNaoFormatadoValido() {
        assertTrue( CEPValidator.isValid("01151000") );
    }

    @Test
    public void testCepInvalidoFormatado() throws Exception {
        assertFalse(CEPValidator.isValid("0T1N1-000"));
    }

    @Test
    public void testCepIvalidoNaoFormatado() throws Exception {
        assertFalse(CEPValidator.isValid("0T1N1000"));
    }

    @Test
    public void testCepIvalidoComEspaco() throws Exception {
        assertFalse( CEPValidator.isValid(" 0T1N1000 ") );
    }

    @Test
    public void testCepValidoComEspaco() throws Exception {
        assertTrue(CEPValidator.isValid(" 01151000 "));
    }

}